=== VNM Vidyard ===
Contributors: indextwo
Tags: vidyard, oembed
Requires at least: 4.6
Tested up to: 5.8.1
Stable tag: 1.0.0
Requires PHP: 7.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Enables standard oEmbed for Vidyard videos using the `share.vidyard.com` link

== Description ==

This is the official contact form/gate plugin for IDC projects. It must be used in conjunction with ACF Pro and Download Monitor in order to work fully. 

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Bing-bang-badonk.

== Changelog ==

= 1.0.0 - 2021-10-01 =
Initial release
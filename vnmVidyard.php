<?php
/*
Plugin Name: VNM Vidyard oEmbed
Plugin URI: https://verynewmedia.com
Description: Enables standard oEmbed for Vidyard videos using the `share.vidyard.com` link
Version: 1.0.0
Author: Lawrie Malen
Author URI: https://verynewmedia.com
License: GPL

Copyright (C) 2021 Lawrie Malen // Very New Media
Released 01-10-2021
Updated: 01-10-2021
*/

if (!defined('ABSPATH')) {
	exit;	//	Exit if accessed directly
}

///
//	Define constants
///

define('VNMVIDYARD', true);
define('VNMVIDYARD_VERSION', '1.0.0');
define('VNMVIDYARD_FILE', __FILE__);
define('VNMVIDYARD_PATH', plugin_dir_path(VNMVIDYARD_FILE));
define('VNMVIDYARD_URL', plugins_url('', VNMVIDYARD_FILE));

define('VNMVIDYARD_JS_PATH', VNMVIDYARD_PATH . '/assets/js/');
define('VNMVIDYARD_JS_URL', VNMVIDYARD_URL . '/assets/js/');

define('VNMVIDYARD_CSS_PATH', VNMVIDYARD_PATH . '/assets/css/');
define('VNMVIDYARD_CSS_URL', VNMVIDYARD_URL . '/assets/css/');

define('VNMVIDYARD_BASENAME', plugin_basename(VNMVIDYARD_FILE));

///
//	Include updates
//	https://github.com/YahnisElsts/plugin-update-checker
///

function vnmVidyard_checkUpdate() {
	require_once(VNMVIDYARD_PATH . 'includes/plugin-update-checker/plugin-update-checker.php');
	
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://bitbucket.org/indextwo/vnmvidyard',
		VNMVIDYARD_FILE,
		'vnmVidyard',
		2
	);
}

add_action('admin_init', 'vnmVidyard_checkUpdate');

///
//	Add the various Vidyard links as oEmbed providers
///

wp_oembed_add_provider(
	'#https?://embed\.vidyard\.com/share/.*#i',
	'https://api.vidyard.com/dashboard/v1.1/oembed',
	true
);

wp_oembed_add_provider(
	'#https?://share\.vidyard\.com/watch/.*#i',
	'https://api.vidyard.com/dashboard/v1.1/oembed',
	true
);

wp_oembed_add_provider(
	'#https?://play\.vidyard\.com/.*#i',
	'https://api.vidyard.com/dashboard/v1.1/oembed',
	true
);

wp_oembed_add_provider(
	'#https?://.*\.hubs\.vidyard\.com/.*#i',
	'https://api.vidyard.com/dashboard/v1.1/oembed',
	true
);

///
//	Enqeue some CSS to prevent the Vidyard videos from being restricted in size
///

function vnmVidyard_enqueueScripts() {
	wp_enqueue_style('vidyard', VNMVIDYARD_CSS_URL . 'vidyard.css', array(), filemtime(VNMVIDYARD_CSS_PATH . 'vidyard.css'));
}

add_action('wp_enqueue_scripts', 'vnmVidyard_enqueueScripts');

?>